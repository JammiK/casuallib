using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Views;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Presenters.Views
{
    public class ResurrectPricePresenter : IResurrectPricePresenter
    {
        public ResurrectPricePresenter([NotNull] IEnumerable<IResurrectPriceView> views,
            [NotNull] IGameSettingsResolver gameSettingsResolver)
        {
            foreach (var view in views)
            {
                view.Text.text = $"{gameSettingsResolver.GameSettings.ResurrectPrice}";
            }
            
        }
    }
}