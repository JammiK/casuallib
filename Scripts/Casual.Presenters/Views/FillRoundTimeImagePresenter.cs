using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Views;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Views
{
    public class FillRoundTimeImagePresenter : IFillRoundTimeImagePresenter
    {
        public FillRoundTimeImagePresenter([NotNull] IEnumerable<IFillRoundTimeImage> images,
            [NotNull] IRoundRx roundRx,
            [NotNull] IGameFacade gameFacade)
        {
            roundRx.TimeForLose
                .Where(_ => gameFacade.State == GameStateConstants.Playing)
                .Subscribe(time =>
                {
                    var fillAmount = time / roundRx.TimeLimit.Value;
                    foreach (var imageHolder in images)
                    {
                        imageHolder.Image.fillAmount = fillAmount;
                    }
                });
        }
        
        
    }
}