using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Views;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Views
{
    public class HardMoneyPresenter : IHardMoneyPresenter
    {
        public HardMoneyPresenter([NotNull] IEnumerable<IHardMoneyView> hardMoneyViews, 
            [NotNull] IPlayerRx playerRx)
        {
            foreach (var view in hardMoneyViews)
            {
                playerRx.Money
                    .Subscribe(money => view.Text.text = money.ToString());
            }
        }
    }
}