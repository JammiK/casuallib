using System;
using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Views;
using CasualLib.Scripts.Casual.Ui.Interfaces.Common;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Presenters.Views
{
    public class ScorePresenter : IScorePresenter
    {
        public ScorePresenter([NotNull] IEnumerable<IScoreView> scoreViews, 
            [NotNull] IEnumerable<IBestScoreView> bestScoreViews,
            [NotNull] IPlayerRx playerRx)
        {
            InitViewsWithStream(scoreViews, playerRx.Score);
            InitViewsWithStream(bestScoreViews, playerRx.BestScore);
        }

        static void InitViewsWithStream(IEnumerable<ITextRxView> views, IObservable<int> stream)
        {
            foreach (var view in views)
            {
                view.SetScoreStream(stream);
            }
        }
    }
}