using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Popups;
using CasualLib.Scripts.Casual.Ui.Interfaces.Popups;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Popups
{
    public class ResurrectPopupPresenter : IResurrectPopupPresenter
    {
        public ResurrectPopupPresenter([NotNull] IResurrectPopup popup,
            [NotNull] IGameEventsSystem gameEventsSystem)
        {
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.ShowResurrectPopup)
                .Subscribe(_ =>
                {
                    popup.GameObject.SetActive(true);
                });
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.Resurrect)
                .Subscribe(_ =>
                {
                    popup.GameObject.SetActive(false);
                });
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.CancelResurrect)
                .Subscribe(_ =>
                {
                    popup.GameObject.SetActive(false);
                });
        }
    }
}