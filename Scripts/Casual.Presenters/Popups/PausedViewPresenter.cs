using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Popups;
using CasualLib.Scripts.Casual.Ui.Interfaces.Popups;
using JetBrains.Annotations;
using UniRx.Triggers;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Popups
{
    public class PausedViewPresenter : IPausedViewPresenter
    {
        public PausedViewPresenter([NotNull] IEnumerable<IPausedView> views,
            [NotNull] IPauseSystem pauseSystem)
        {
            foreach (var view in views)
            {
                view.GameObject.OnEnableAsObservable()
                    .Subscribe(_ => pauseSystem.Pause());

                view.GameObject.OnDisableAsObservable()
                    .Subscribe(_ => pauseSystem.Resume());
            }
        }
    }
}