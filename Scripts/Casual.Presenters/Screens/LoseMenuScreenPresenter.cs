using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Ui.Interfaces.Common;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Presenters.Screens
{
    public class LoseMenuScreenPresenter : BaseScreenPresenter
    {
        public LoseMenuScreenPresenter(IEnumerable<IGameObjectHolder> views,
            [NotNull] IGameFacadeRx gameStateHolderRx) : base(views, gameStateHolderRx)
        {
        }

        protected override int Screen => GameStateConstants.LoseMenu;
    }
}