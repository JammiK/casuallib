using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Screens;
using CasualLib.Scripts.Casual.Ui.Interfaces.Common;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Screens
{
    public abstract class BaseScreenPresenter : IScreenPresenter
    {
        protected BaseScreenPresenter(IEnumerable<IGameObjectHolder> views,
            [NotNull] IGameStateHolderRx gameStateHolderRx)
        {
            foreach (var view in views)
            {
                gameStateHolderRx.State
                    .Subscribe(state => 
                        view.GameObject.SetActive(state == Screen));

            }
        }
        
        protected abstract int Screen { get; }
    }
}