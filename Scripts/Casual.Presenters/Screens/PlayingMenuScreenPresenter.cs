using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Ui.Interfaces.Screens;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Presenters.Screens
{
    public class PlayingMenuScreenPresenter : BaseScreenPresenter
    {
        public PlayingMenuScreenPresenter(IEnumerable<IPlayingMenuView> views,
            [NotNull] IGameFacadeRx gameStateHolderRx) 
            : base(views, gameStateHolderRx)
        {
        }

        protected override int Screen => GameStateConstants.Playing;
    }
}