using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Buttons;
using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Presenters.Buttons
{
    public class ResurrectButtonPresenter : ButtonPresenterBase<IResurrectButton>, IResurrectButtonPresenter
    {
        readonly IGameEventsSystem _gameEventsSystem;
        
        public ResurrectButtonPresenter([NotNull] IEnumerable<IResurrectButton> buttons,
            [NotNull] IGameEventsSystem gameEventsSystem) : base(buttons)
        {
            _gameEventsSystem = gameEventsSystem;
        }

        protected override void OnClickAction()
        {
            _gameEventsSystem.GameEvent.OnNext(GameEvents.Resurrect);
        }
    }
}