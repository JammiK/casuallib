using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Buttons;
using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Buttons
{
    public class StartGameButtonPresenter : ButtonPresenterBase<IStartGameButton>, IStartGameButtonPresenter
    {
        readonly IGameStateSystem _gameStateSystem;
        
        public StartGameButtonPresenter([NotNull] IEnumerable<IStartGameButton> buttons,
            [NotNull] IGameStateSystem gameStateSystem) : base(buttons)
        {
            _gameStateSystem = gameStateSystem;
        }

        protected override void OnClickAction()
        {
            _gameStateSystem.StartGame();
        }
    }
}