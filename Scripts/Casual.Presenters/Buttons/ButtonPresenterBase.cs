using System.Collections.Generic;
using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Buttons
{
    public abstract class ButtonPresenterBase<T> where T : IButtonHolder
    {
        protected ButtonPresenterBase(IEnumerable<T> buttons)
        {
            foreach (var boardButton in buttons)
            {
                boardButton.Button
                    .onClick
                    .AsObservable()
                    .Subscribe(_ => OnClickAction());
            }
        }

        protected abstract void OnClickAction();
    }
}