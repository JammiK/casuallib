using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Socials;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Buttons;
using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Presenters.Buttons
{
    public class ShowLeaderBoardButtonPresenter :ButtonPresenterBase<IShowLeaderBoardButton>, IShowLeaderBoardButtonPresenter
    {
        readonly ILeaderBoardSystem _leaderBoardSystem;
        
        public ShowLeaderBoardButtonPresenter([NotNull] ILeaderBoardSystem leaderBoardSystem,
            [NotNull] IEnumerable<IShowLeaderBoardButton> boardButtons)
        :base(boardButtons)
        {
            _leaderBoardSystem = leaderBoardSystem;
        }

        protected override void OnClickAction()
        {
            _leaderBoardSystem.ShowLeaderBoard();
        }
    }
}