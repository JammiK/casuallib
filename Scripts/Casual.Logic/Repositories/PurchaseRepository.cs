using System.Collections.Generic;
using System.Linq;
using CasualLib.Scripts.Casual.Data.Interfaces.Purchase;
using CasualLib.Scripts.Casual.Logic.Interfaces.Repositories;
using JetBrains.Annotations;
using UnityEngine.Purchasing;

namespace CasualLib.Scripts.Casual.Logic.Repositories
{
    public class PurchaseRepository : IPurchaseRepository
    {
        readonly IPurchaseContainer _purchaseContainer;

        public PurchaseRepository([NotNull] IPurchaseContainer purchaseContainer)
        {
            _purchaseContainer = purchaseContainer;
        }

        public IEnumerable<ProductDefinition> GetProductDefinition()
        {
            return _purchaseContainer.PurchaseEntities
                .Select(entity => new ProductDefinition(entity.Id, GetProductType(entity.PurchaseType)));
        }

        ProductType GetProductType(int internalType)
        {
            switch (internalType)
            {
                case PurchaseTypes.Consumable:
                    return ProductType.Consumable;
                case PurchaseTypes.NonConsumable:
                    return ProductType.NonConsumable;
                
                default:
                    return ProductType.Consumable;
            }
        }
    }
}