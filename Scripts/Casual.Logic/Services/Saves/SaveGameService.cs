using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.Saves;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Services.Saves
{
    public class SaveGameService : ISaveGameService
    {
        const string PlayerPrefsSaveKey = "Game";
        readonly IGameFacade _gameFacade;

        public SaveGameService([NotNull] IGameFacade gameFacade)
        {
            _gameFacade = gameFacade;
        }

        public void Save()
        {
            var json = JsonConvert.SerializeObject(_gameFacade);
            PlayerPrefs.SetString(PlayerPrefsSaveKey, json);
        }

        public void Load()
        {
            var json = PlayerPrefs.GetString(PlayerPrefsSaveKey);
            JsonConvert.PopulateObject(json, _gameFacade);
        }
    }
}