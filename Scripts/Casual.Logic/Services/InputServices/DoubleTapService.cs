using System;
using System.Collections.Generic;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.InputServices;
using UniRx;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Services.InputServices
{
    public class DoubleTapService : IDoubleTapService
    {
        const int DelayMsValue = 250;
        
        readonly TimeSpan Delay = TimeSpan.FromMilliseconds(DelayMsValue);
        
        public DoubleTapService(ITapService tapService)
        {
            DoubleTap = tapService.Tap
                .Buffer(tapService.Tap.Throttle(Delay))
                .Where(xs => xs.Count >= 2);
        }
        
        public IObservable<IList<Vector2>> DoubleTap { get; }
    }
}