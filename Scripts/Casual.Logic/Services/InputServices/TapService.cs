using System;
using System.Linq;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.InputServices;
using UniRx;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Services.InputServices
{
    public class TapService : ITapService
    {
        public TapService()
        {
            Tap = Observable.EveryFixedUpdate()
                .Where(_ => Input.touchCount > 0)
                .Select(_ => Input.touches.First())
                .Where(tap => tap.phase == TouchPhase.Began)
                .Select(tap => tap.position);
        }
        
        public IObservable<Vector2> Tap { get; }
    }
}