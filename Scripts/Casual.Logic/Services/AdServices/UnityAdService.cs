using CasualLib.Scripts.Casual.Logic.Interfaces.Services.AdServices;
using UnityEngine;
using UnityEngine.Advertisements;

namespace CasualLib.Scripts.Casual.Logic.Services.AdServices
{
    public class UnityAdService : IAdService
    {
        string _gameId;
        public UnityAdService()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    _gameId = "3109897";
                    break;
                case RuntimePlatform.IPhonePlayer:
                    _gameId = "3109896";
                    break;
            }
            
            Advertisement.Initialize (_gameId, true);
        }

        public bool IsReady(string placement)
        {
            return Advertisement.IsReady(placement);
        }

        public void Show(string placement)
        {
            Advertisement.Show(placement);
        }
    }
}