using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.AdServices;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.AdSystems;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.AdSystems
{
    public class AdSystem : IAdSystem
    {
        const string BannerPlacement = "bottonBanner";
        const string VideoPlacement = "video";
        
        public AdSystem([NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] IAdService adService,
            [NotNull] IGameFacade gameFacade)
        {
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameEnd)
                .Where(_ => !gameFacade.AdRemoved)
                .Subscribe(_ => adService.Show(BannerPlacement));
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameEnd)
                .Where(_ => adService.IsReady(VideoPlacement))
                .Where(_ => !gameFacade.AdRemoved)
                .Subscribe(_ => adService.Show(VideoPlacement));
        }
    }
}