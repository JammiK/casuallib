using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.PlayerSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.PlayerSystems
{
    public class ScoreSystem : IScoreSystem
    {
        public ScoreSystem([NotNull] IPlayerRx playerRx,
            [NotNull] IGameEventsSystem gameEventsSystem)
        {
            BindBestScore(playerRx);
            BindScore(gameEventsSystem, playerRx);
        }

        static void BindBestScore(IScoreHolderRx playerRx)
        {
            playerRx.Score
                .Where(score => score > playerRx.BestScore.Value)
                .Subscribe(score => playerRx.BestScore.Value = score);
        }

        static void BindScore(IGameEventsSystem gameEventsSystem, IScoreHolderRx playerRx)
        {
            gameEventsSystem.GameEvent
                .Where(playerEvent => playerEvent == GameEvents.NextRound)
                .Subscribe(_ => playerRx.Score.Value++);

            gameEventsSystem.GameEvent
                .Where(playerEvent => playerEvent == GameEvents.GameEnd)
                .Subscribe(_ => playerRx.Score.Value = 0);
        }
    }
}