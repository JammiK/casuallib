using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.PlayerSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.PlayerSystems
{
    public class HardMoneySystem : IHardMoneySystem
    {
        [NotNull] readonly IPlayerRx _playerRx;
        [NotNull] readonly IGameEventsSystem _gameEventsSystem;
        [NotNull] readonly IGameSettings _gameSettings;

        public HardMoneySystem([NotNull] IPlayerRx playerRx,
            [NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] IGameSettingsResolver gameSettingsResolver)
        {
            _playerRx = playerRx;
            _gameEventsSystem = gameEventsSystem;
            _gameSettings = gameSettingsResolver.GameSettings;
            BindNextRoundIncrement();
            BindDecreaseResurrect();
        }

        void BindNextRoundIncrement()
        {
            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.NextRound)
                .Subscribe(_ => _playerRx.Money.Value++ );
        }

        void BindDecreaseResurrect()
        {
            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.Resurrect)
                .Subscribe(_ => _playerRx.Money.Value -= _gameSettings.ResurrectPrice );
        }
    }
}