using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Socials;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Systems.Socials
{
    public class SocialSystem : ISocialSystem
    {
        public SocialSystem()
        {
            Social.localUser.Authenticate(_ => { });
        }

        public bool Authenticated()
        {
            return Social.localUser.authenticated;
        }
    }
}