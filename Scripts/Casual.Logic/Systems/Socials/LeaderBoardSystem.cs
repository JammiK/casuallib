using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Socials;
using JetBrains.Annotations;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Systems.Socials
{
    public class LeaderBoardSystem : ILeaderBoardSystem
    {
        readonly ISocialSystem _socialSystem;

        public LeaderBoardSystem([NotNull] ISocialSystem socialSystem)
        {
            _socialSystem = socialSystem;
        }

        public void ShowLeaderBoard()
        {
            if (_socialSystem.Authenticated())
            {
                Social.ShowLeaderboardUI();
            }
        }
    }
}