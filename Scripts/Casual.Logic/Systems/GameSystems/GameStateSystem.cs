using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class GameStateSystem : IGameStateSystem
    {
        readonly IGameStateHolderRx _gameStateHolder;
        readonly IGameEventsSystem _gameEventsSystem;

        public GameStateSystem([NotNull] IGameFacadeRx _gameFacadeRx,
            [NotNull] IGameEventsSystem gameEventsSystem)
        {
            _gameStateHolder = _gameFacadeRx;
            _gameEventsSystem = gameEventsSystem;
        }

        public void StartGame()
        {
            _gameStateHolder.State.Value = GameStateConstants.Playing;
            _gameEventsSystem.GameEvent.OnNext(GameEvents.GameStarted);
        }

        public void LoseGame()
        {
            _gameStateHolder.State.Value = GameStateConstants.LoseMenu;
            _gameEventsSystem.GameEvent.OnNext(GameEvents.Lose);
        }

        public void OpenMainMenu()
        {
            _gameStateHolder.State.Value = GameStateConstants.MainMenu;
            _gameEventsSystem.GameEvent.OnNext(GameEvents.MainMenuOpened);
        }

        public void Resume()
        {
            _gameStateHolder.State.Value = GameStateConstants.Playing;
        }
    }
}