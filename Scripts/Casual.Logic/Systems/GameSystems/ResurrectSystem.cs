using System;
using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class ResurrectSystem : IResurrectSystem
    {
        readonly IGameStateSystem _gameStateSystem;
        readonly IGameEventsSystem _gameEventsSystem;
        readonly IGameSettings _gameSettings;
        [NotNull] readonly IPlayerRx _playerRx;

        IDisposable _timer;
        bool _timerStarted;
        
        
        public ResurrectSystem([NotNull] IGameStateSystem gameStateSystem,
            [NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] IGameSettingsResolver gameSettingsResolver,
            [NotNull] IPlayerRx playerRx)
        {
            _gameStateSystem = gameStateSystem;
            _gameEventsSystem = gameEventsSystem;
            _gameSettings = gameSettingsResolver.GameSettings;
            _playerRx = playerRx;

            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.Resurrect)
                .Subscribe(_ => Resurrect());

            BindStopCancelResurrectTimer();
            BindResurrectRequest();
        }

        public void Resurrect()
        {
            _gameStateSystem.Resume();
            _gameEventsSystem.GameEvent.OnNext(GameEvents.NextRound);
        }

        void CancelResurrect()
        {
            _gameEventsSystem.GameEvent.OnNext(GameEvents.CancelResurrect);
        }

        void BindResurrectRequest()
        {
            var resurrectRequestStream = _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.RequestResurrect);

            var hasEnoughMoneyStream = resurrectRequestStream
                .Where(_ => _playerRx.Money.Value >= _gameSettings.ResurrectPrice);
            
            resurrectRequestStream.Where(_ => _playerRx.Money.Value >= _gameSettings.ResurrectPrice)
                .Subscribe(_ => ResurrectPopupAction());
            
            resurrectRequestStream.Where(_ => _playerRx.Money.Value < _gameSettings.ResurrectPrice)
                .Subscribe(_ => CancelResurrect());
        }

        void ResurrectPopupAction()
        {
            _gameEventsSystem.GameEvent.OnNext(GameEvents.ShowResurrectPopup);
            StartCancelResurrectTimer();
        }

        void StartCancelResurrectTimer()
        {
            _timer = Observable.Range(0, 1)
                .Delay(TimeSpan.FromSeconds(_gameSettings.ResurrectTimeLimit))
                .Subscribe(_ => CancelResurrect());

            _timerStarted = true;
        }

        void BindStopCancelResurrectTimer()
        {
            _gameEventsSystem.GameEvent
                .Where(_ => _timerStarted)
                .Where(ev => ev == GameEvents.Resurrect || ev == GameEvents.CancelResurrect)
                .Subscribe(_ =>
                {
                    _timer?.Dispose();
                    _timer = null;
                    _timerStarted = false;
                });
        }
    }
}