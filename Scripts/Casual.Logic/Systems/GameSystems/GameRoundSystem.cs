using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class GameRoundSystem : IGameRoundSystem
    {
        public GameRoundSystem([NotNull] IGameFacade gameFacade,
            [NotNull] IGameEventsSystem gameEventsSystem)
        {
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameStarted)
                .Subscribe(_ => gameFacade.Round.Rx.Index.Value = 1);
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.NextRound)
                .Where(_ => gameFacade.Rx.State.Value == GameStateConstants.Playing)
                .Subscribe(_ => gameFacade.Round.Rx.Index.Value++);
        }
    }
}