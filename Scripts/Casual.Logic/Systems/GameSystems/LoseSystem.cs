using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class LoseSystem : ILoseSystem
    {
        readonly IGameSettings _gameSettings;
        readonly IGameFacadeRx _gameFacadeRx;
        readonly IGameEventsSystem _gameEventsSystem;

        public LoseSystem([NotNull] IGameFacadeRx gameFacadeRx,
            [NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] IGameSettingsResolver gameSettingsResolver)
        {
            _gameSettings = gameSettingsResolver.GameSettings;
            _gameFacadeRx = gameFacadeRx;
            _gameEventsSystem = gameEventsSystem;
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.Lose)
                .Subscribe(_ => LoseInternal());
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.CancelResurrect)
                .Subscribe(_ => EndGame());
        }

        void LoseInternal()
        {
            if (_gameSettings.CanResurrect)
            {
                ResurrectRequest();
            }
            else
            {
                EndGame();
            }
        }

        void EndGame()
        {
            ChangeGameState();
            _gameEventsSystem.GameEvent.OnNext(GameEvents.GameEnd);
        }

        void ResurrectRequest()
        {
            _gameEventsSystem.GameEvent.OnNext(GameEvents.RequestResurrect);
        }

        void ChangeGameState()
        {
            _gameFacadeRx.State.Value = _gameSettings.LoseMenuEnable
                ? GameStateConstants.LoseMenu
                : GameStateConstants.MainMenu;
        }
    }
}