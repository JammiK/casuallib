using System;
using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class RoundTimeLimitSystem : IRoundTimeLimitSystem
    {
        readonly IGameSettings _gameSettings;
        readonly IRoundRx _roundHolder;
        readonly IGameEventsSystem _gameEventsSystem;
        readonly IPauseSystem _pauseSystem;

        IDisposable _timer;
        
        public RoundTimeLimitSystem([NotNull] IGameSettingsResolver gameSettingsResolver,
            [NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] IGameFacade gameFacade,
            [NotNull] IPauseSystem pauseSystem)
        {
            _gameSettings = gameSettingsResolver.GameSettings;
            _roundHolder = gameFacade.Round.Rx;
            _gameEventsSystem = gameEventsSystem;
            _pauseSystem = pauseSystem;

            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameEnd)
                .Subscribe(_ => StopTimer());

            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameStarted)
                .Subscribe(_ => StartTimer());
            
            _gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.NextRound)
                .Subscribe(_ => ResetTimer());
        }

        void StartTimer()
        {
            if (!_gameSettings.RoundTimeLimited)
            {
                return;
            }
            
            ResetTimer();
            _timer = Observable.EveryFixedUpdate()
                .Where(_ => !_pauseSystem.IsPause)
                .Subscribe(_ =>
                {
                    UpdateTime();
                });
        }
        
        void ResetTimer()
        {
            _roundHolder.TimeLimit.Value = GetTimeForCurrentRound();
            _roundHolder.TimeForLose.Value = _roundHolder.TimeLimit.Value;
        }

        void StopTimer()
        {
            _timer?.Dispose();   
        }

        float GetTimeForCurrentRound()
        {
            return (float) _gameSettings.RoundMaxTime.TotalSeconds
                                   * Mathf.Pow(_gameSettings.RoundScale, _roundHolder.Index.Value -1);
        }

        void UpdateTime()
        {
            _roundHolder.TimeForLose.Value -= Time.deltaTime;
            CheckLoseGame();
        }
        
        void CheckLoseGame()
        {
            if (_roundHolder.TimeForLose.Value < 0)
            {
                _gameEventsSystem.GameEvent
                    .OnNext(GameEvents.Lose);
            }
        }
    }
}