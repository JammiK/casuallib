using System;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class PauseSystem : IPauseSystem
    {
        ReactiveProperty<int> _pauseCounter = new ReactiveProperty<int>(0);

        public bool IsPause { get; private set; }

        public PauseSystem()
        {
            _pauseCounter.Where(value => value < 0)
                .Subscribe(_ =>
                    throw new InvalidOperationException($"Pause counter emit value < 0"));
            
            _pauseCounter.Subscribe(value => IsPause = value > 0);
        }

        public void Pause()
        {
            _pauseCounter.Value++;
        }

        public void Resume()
        {
            _pauseCounter.Value--;
        }
    }
}