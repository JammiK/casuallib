using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.GameSystems
{
    public class GameEventsSystem : IGameEventsSystem
    {
        public Subject<int> GameEvent { get; } = new Subject<int>();
    }
}