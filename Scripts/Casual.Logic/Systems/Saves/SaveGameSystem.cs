using CasualLib.Scripts.Casual.Logic.Interfaces.Services.Saves;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Saves;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.Saves
{
    public class SaveGameSystem : ISaveGameSystem
    {
        public SaveGameSystem([NotNull] IGameEventsSystem gameEventsSystem,
            [NotNull] ISaveGameService saveGameService)
        {
            saveGameService.Load();
            
            gameEventsSystem.GameEvent
                .Where(ev => ev == GameEvents.GameEnd)
                .Subscribe(_ => saveGameService.Save());
        }
    }
}