using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Purchase;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Purchase;
using JetBrains.Annotations;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.Purchase
{
    public class RemoveAdPurchaseCompleteListener : IPurchaseCompleteListener
    {
        public RemoveAdPurchaseCompleteListener([NotNull] IPurchaseSystem purchaseSystem,
            IGameFacade gameFacade)
        {
            purchaseSystem.PurchaseComplete
                .Where(purchase => purchase == Purchases.RemoveAdId)
                .Subscribe(_ => gameFacade.AdRemoved = true);
        }
    }
}