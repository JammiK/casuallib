using CasualLib.Scripts.Casual.Logic.Interfaces.Repositories;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Purchase;
using JetBrains.Annotations;
using UniRx;
using UnityEngine.Purchasing;

namespace CasualLib.Scripts.Casual.Logic.Systems.Purchase
{
    public class PurchaseSystem : IPurchaseSystem, IStoreListener
    {
        public PurchaseSystem([NotNull] IPurchaseRepository purchaseRepository)
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            var products = purchaseRepository.GetProductDefinition();
            builder.AddProducts(products);

            UnityPurchasing.Initialize (this, builder);
        }

        public Subject<string> PurchaseComplete { get; } = new Subject<string>();
        public ReactiveProperty<bool> IsInitialized { get; } = new ReactiveProperty<bool>(false);

        IStoreController _controller;
        IExtensionProvider _extensionProvider;
        
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            PurchaseComplete.OnNext(e.purchasedProduct.definition.id);
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _controller = controller;
            _extensionProvider = extensions;
            IsInitialized.Value = true;
        }
        
        public void InitiatePurchase(string productId)
        {
            if (!IsInitialized.Value)
            {
                return;
            }
            
            _controller.InitiatePurchase(productId);
        }
    }
}