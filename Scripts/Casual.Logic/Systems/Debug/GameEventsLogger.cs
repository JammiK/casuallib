using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Debug;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Systems.Debug
{
    public class GameEventsLogger : IGameEventsLogger
    {
        public GameEventsLogger(IGameEventsSystem gameEventsSystem)
        {
            gameEventsSystem.GameEvent.Subscribe(ev => UnityEngine.Debug.Log($"Game Event: {ev}"));
        }
    }
}