using System;
using UniRx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game
{
    public interface IGameSettings
    {
        bool RoundTimeLimited { get; set; }
        TimeSpan RoundMaxTime { get; set; }
        float RoundScale { get; set; }
        bool LoseMenuEnable { get; set; }
        bool CanResurrect { get; set; }
        float ResurrectTimeLimit { get; set; }
        int ResurrectPrice { get; set; }
    }
}