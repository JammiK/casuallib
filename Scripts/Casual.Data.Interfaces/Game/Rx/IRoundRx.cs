using UniRx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx
{
    public interface IRoundRx
    {
        ReactiveProperty<float> TimeLimit { get; }
        ReactiveProperty<float> TimeForLose { get; }
        ReactiveProperty<int> Index { get; }
    }
}