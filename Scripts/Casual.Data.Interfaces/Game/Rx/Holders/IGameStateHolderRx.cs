using UniRx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders
{
    public interface IGameStateHolderRx
    {
        ReactiveProperty<int> State { get; }
    }
}