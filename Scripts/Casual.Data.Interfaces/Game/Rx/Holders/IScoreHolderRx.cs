using UniRx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders
{
    public interface IScoreHolderRx
    {
        ReactiveProperty<int> Score { get; }
        ReactiveProperty<int> BestScore { get; }
    }
}