namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders
{
    public interface IRoundHolderRx
    {   
        IRound Round { get; }
    }
}