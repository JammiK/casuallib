using UniRx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders
{
    public interface IMoneyHolderRx
    {
        ReactiveProperty<int> Money { get; }
    }
}