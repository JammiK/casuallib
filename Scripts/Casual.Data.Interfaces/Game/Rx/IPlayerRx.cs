using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx.Holders;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx
{
    public interface IPlayerRx : IScoreHolderRx, IMoneyHolderRx
    {
        
    }
}