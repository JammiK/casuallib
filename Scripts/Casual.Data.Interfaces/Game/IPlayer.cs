using CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game
{
    public interface IPlayer : IScoreHolder, IMoneyHolder
    {
        IPlayerRx Rx { get; }
    }
}