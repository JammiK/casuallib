namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants
{
    public class GameStateConstants
    {
        public const int MainMenu = 1;
        public const int Playing = 2;
        public const int LoseMenu = 3;
    }
}