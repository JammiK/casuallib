using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game
{
    public interface IRound
    {
        IRoundRx Rx { get; }
        int Index { get; set; }
        float TimeLimit { get; set; }
        float TimeForLose { get; set; }
    }
}