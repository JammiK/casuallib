using CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Game
{
    public interface IGameFacade : IPlayerHolder, IGameStateHolder, IRoundHolder
    {
        IGameFacadeRx Rx { get; }
        bool AdRemoved { get; set; }
    }
}