namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders
{
    public interface IPlayerHolder
    {
        IPlayer Player { get; }
    }
}