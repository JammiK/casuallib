namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders
{
    public interface IRoundHolder
    {
        IRound Round { get; }
    }
}