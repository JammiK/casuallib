namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders
{
    public interface IGameStateHolder
    {
        int State { get; set; }
    }
}