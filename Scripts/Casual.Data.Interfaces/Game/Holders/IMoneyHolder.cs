namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders
{
    public interface IMoneyHolder
    {
        int Money { get; set; }
    }
}