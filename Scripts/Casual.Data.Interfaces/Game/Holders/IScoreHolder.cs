namespace CasualLib.Scripts.Casual.Data.Interfaces.Game.Holders
{
    public interface IScoreHolder
    {
        int Score { get; set; }
        int BestScore { get; set; }
    }
}