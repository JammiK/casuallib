namespace CasualLib.Scripts.Casual.Data.Interfaces.Purchase
{
    public class PurchaseTypes
    {
        public const int Consumable = 1;
        public const int NonConsumable = 2;
    }
}