using System.Collections.Generic;

namespace CasualLib.Scripts.Casual.Data.Interfaces.Purchase
{
    public interface IPurchaseContainer
    {
        IEnumerable<IPurchaseEntity> PurchaseEntities { get; }
    }
}