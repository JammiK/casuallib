namespace CasualLib.Scripts.Casual.Data.Interfaces.Purchase
{
    public interface IPurchaseEntity
    {
        string Id { get; }
        int PurchaseType { get; }
        bool Enabled { get; }
    }
}