namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Socials
{
    public interface ISocialSystem
    {
        bool Authenticated();
    }
}