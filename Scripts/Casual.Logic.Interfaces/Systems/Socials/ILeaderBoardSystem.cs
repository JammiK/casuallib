namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Socials
{
    public interface ILeaderBoardSystem
    {
        void ShowLeaderBoard();
    }
}