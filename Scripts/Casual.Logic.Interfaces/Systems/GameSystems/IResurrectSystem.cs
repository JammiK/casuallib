namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public interface IResurrectSystem
    {
        void Resurrect();
    }
}