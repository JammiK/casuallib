using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public interface IGameEventsSystem
    {
        Subject<int> GameEvent { get; }
    }
}