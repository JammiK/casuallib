using System;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public interface IPauseSystem
    {
        bool IsPause { get; }
        void Pause();
        void Resume();
    }
}