namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public class GameEvents
    {
        public const int MainMenuOpened = 1;
        public const int GameStarted = 2;
        public const int Lose = 3;
        public const int NextRound = 4;
        public const int GameEnd = 5;
        public const int Resurrect = 6;
        public const int CancelResurrect = 7;
        public const int RequestResurrect = 8;
        public const int ShowResurrectPopup = 9;
    }
}