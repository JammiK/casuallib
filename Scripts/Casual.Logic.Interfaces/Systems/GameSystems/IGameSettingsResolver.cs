using CasualLib.Scripts.Casual.Data.Interfaces.Game;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public interface IGameSettingsResolver
    {
        IGameSettings GameSettings { get; }
    }
}