namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems
{
    public interface IGameStateSystem
    {
        void StartGame();
        void LoseGame();
        void OpenMainMenu();
        void Resume();
    }
}