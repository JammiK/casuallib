using UniRx;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Purchase
{
    public interface IPurchaseSystem
    {
        void InitiatePurchase(string productId);
        
        Subject<string> PurchaseComplete { get; }
        ReactiveProperty<bool> IsInitialized { get; }
    }
}