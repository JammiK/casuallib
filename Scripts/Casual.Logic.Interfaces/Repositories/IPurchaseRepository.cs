using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Repositories
{
    public interface IPurchaseRepository
    {
        IEnumerable<ProductDefinition> GetProductDefinition();
    }
}