using System;
using System.Collections.Generic;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Services.InputServices
{
    public interface IDoubleTapService
    {
        IObservable<IList<Vector2>> DoubleTap { get; }
    }
}