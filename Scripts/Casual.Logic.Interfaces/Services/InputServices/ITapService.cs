using System;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Logic.Interfaces.Services.InputServices
{
    public interface ITapService
    {
        IObservable<Vector2> Tap { get; }
    }
}