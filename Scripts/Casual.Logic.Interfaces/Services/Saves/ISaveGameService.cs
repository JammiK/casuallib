namespace CasualLib.Scripts.Casual.Logic.Interfaces.Services.Saves
{
    public interface ISaveGameService
    {
        void Save();
        void Load();
    }
}