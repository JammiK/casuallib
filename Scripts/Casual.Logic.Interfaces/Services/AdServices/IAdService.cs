namespace CasualLib.Scripts.Casual.Logic.Interfaces.Services.AdServices
{
    public interface IAdService
    {
        bool IsReady(string placement);
        void Show(string placement);
    }
}