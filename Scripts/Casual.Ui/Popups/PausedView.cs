using CasualLib.Scripts.Casual.Ui.Interfaces.Popups;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.Popups
{
    public class PausedView : MonoBehaviour, IPausedView
    {
        public GameObject GameObject => gameObject;
    }
}