using CasualLib.Scripts.Casual.Ui.Interfaces.Popups;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.Popups
{
    public class ResurrectPopup : MonoBehaviour, IResurrectPopup
    {
        public GameObject GameObject => gameObject;
    }
}