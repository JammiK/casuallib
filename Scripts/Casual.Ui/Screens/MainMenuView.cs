using CasualLib.Scripts.Casual.Ui.Interfaces.Screens;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.Screens
{
    public class MainMenuView : MonoBehaviour, IMainMenuView
    {
        GameObject _gameObject;

        public GameObject GameObject
        {
            get
            {
                if (_gameObject == null)
                {
                    _gameObject = gameObject;
                }

                return _gameObject;
            }
        }
    }
}