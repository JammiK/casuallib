using CasualLib.Scripts.Casual.Ui.Interfaces.Screens;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.Screens
{
    public class PlayingMenuView : MonoBehaviour, IPlayingMenuView
    {
        GameObject _gameObject;

        public GameObject GameObject
        {
            get
            {
                if (_gameObject == null)
                {
                    _gameObject = gameObject;
                }

                return _gameObject;
            }
        }
    }
}