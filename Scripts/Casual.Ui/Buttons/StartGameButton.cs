using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.Buttons
{
    public class StartGameButton : MonoBehaviour, IStartGameButton
    {
        [SerializeField] Button _button;

        public Button Button => _button;
    }
}