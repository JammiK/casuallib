using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.Buttons
{
    public class ResurrectButton : MonoBehaviour, IResurrectButton
    {
        [SerializeField] Button _button;
        public Button Button => _button;
    }
}