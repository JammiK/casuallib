using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.Buttons
{
    public class ShowLeaderBoardButton : MonoBehaviour, IShowLeaderBoardButton
    {
        [SerializeField] Button _button;

        public Button Button => _button;
    }
}