using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using TMPro;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.View
{
    public class ResurrectPriceView : MonoBehaviour, IResurrectPriceView
    {
        [SerializeField]
        TextMeshProUGUI _text;

        public TextMeshProUGUI Text => _text;
    }
}