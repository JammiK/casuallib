using System;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using TMPro;
using UniRx;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.View
{
    public class ScoreView : MonoBehaviour, IScoreView
    {
        [SerializeField] TextMeshProUGUI _scoreText;
        [SerializeField] string _prefix;
        [SerializeField] string _postfix;
        
        public void SetScoreStream(IObservable<int> score)
        {
            if (_scoreText == null)
            {
                throw new InvalidOperationException(
                    $"Unable to bind to '{nameof(TextMeshProUGUI)}'. Variable is null.");
            }
            
            score.Subscribe(val => _scoreText.text = $"{_prefix}{val}{_postfix}");
        }
    }
}