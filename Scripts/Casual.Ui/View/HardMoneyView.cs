using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using TMPro;
using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.View
{
    public class HardMoneyView : MonoBehaviour, IHardMoneyView
    {
        [SerializeField] TextMeshProUGUI _text;

        public TextMeshProUGUI Text => _text;
    }
}