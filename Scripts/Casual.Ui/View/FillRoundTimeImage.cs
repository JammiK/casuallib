using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using UnityEngine;
using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.View
{
    public class FillRoundTimeImage : MonoBehaviour, IFillRoundTimeImage
    {
        [SerializeField] Image _image;

        public Image Image => _image;
    }
}