using CasualLib.Scripts.Casual.Data.Interfaces.Purchase;

namespace CasualLib.Scripts.Casual.Data.Purchase
{
    public class PurchaseEntity : IPurchaseEntity
    {
        public PurchaseEntity(string id, int purchaseType, bool enabled)
        {
            Id = id;
            PurchaseType = purchaseType;
            Enabled = enabled;
        }

        public string Id { get; }
        public int PurchaseType { get; }
        public bool Enabled { get; }
    }
}