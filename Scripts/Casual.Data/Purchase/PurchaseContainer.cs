using System.Collections.Generic;
using CasualLib.Scripts.Casual.Data.Interfaces.Purchase;

namespace CasualLib.Scripts.Casual.Data.Purchase
{
    public class PurchaseContainer : IPurchaseContainer
    {
        public IEnumerable<IPurchaseEntity> PurchaseEntities { get; }
        
        public PurchaseContainer()
        {
            PurchaseEntities = new[]
            {
                new PurchaseEntity(Purchases.RemoveAdId, PurchaseTypes.NonConsumable, true), 
            };
        }

    }
}