using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using UniRx;

namespace CasualLib.Scripts.Casual.Data.Game.Rx
{
    public class PlayerRx : IPlayerRx
    {
        public PlayerRx(int score, int bestScore, int money)
        {
            Score = new ReactiveProperty<int>(score);
            BestScore = new ReactiveProperty<int>(bestScore);
            Money = new ReactiveProperty<int>(money);
        } 
        
        public ReactiveProperty<int> Score { get; }
        public ReactiveProperty<int> BestScore { get; }
        public ReactiveProperty<int> Money { get; }
    }
}