using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using UniRx;

namespace CasualLib.Scripts.Casual.Data.Game.Rx
{
    public class GameFacadeRx : IGameFacadeRx
    {
        public GameFacadeRx(int state)
        {
            State = new ReactiveProperty<int>(state);
        }

        public ReactiveProperty<int> State { get; }
    }
}