using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using UniRx;

namespace CasualLib.Scripts.Casual.Data.Game.Rx
{
    public class RoundRx : IRoundRx
    {
        public ReactiveProperty<float> TimeLimit { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> TimeForLose { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<int> Index { get; } = new ReactiveProperty<int>();
    }
}