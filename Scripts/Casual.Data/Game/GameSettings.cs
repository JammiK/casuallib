using System;
using CasualLib.Scripts.Casual.Data.Interfaces.Game;

namespace CasualLib.Scripts.Casual.Data.Game
{
    public class GameSettings : IGameSettings
    {
        public bool RoundTimeLimited { get; set; } = false;
        public TimeSpan RoundMaxTime { get; set; } = TimeSpan.FromMinutes(1);
        public float RoundScale { get; set; } = 1f;
        public bool LoseMenuEnable { get; set; } = false;
        public bool CanResurrect { get; set; } = false;
        public float ResurrectTimeLimit { get; set; } = 4f;
        public int ResurrectPrice { get; set; } = 1;
    }
}