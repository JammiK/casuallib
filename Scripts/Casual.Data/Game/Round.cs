using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace CasualLib.Scripts.Casual.Data.Game
{
    public class Round : IRound
    {
        public Round([NotNull] IRoundRx rx)
        {
            Rx = rx;
        }

        [JsonIgnore] public IRoundRx Rx { get; }
        public int Index
        {
            get => Rx.Index.Value;
            set => Rx.Index.Value = value;
        }

        public float TimeLimit
        {
            get => Rx.TimeLimit.Value;
            set => Rx.TimeLimit.Value = value;
        }

        public float TimeForLose
        {
            get => Rx.TimeForLose.Value;
            set => Rx.TimeForLose.Value = value;
        }
    }
}