using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace CasualLib.Scripts.Casual.Data.Game
{
    [JsonObject(MemberSerialization.OptIn)]
    public class GameFacade : IGameFacade
    {
        public GameFacade([NotNull] IPlayer player,
            [NotNull] IGameFacadeRx rx,
            [NotNull] IRound round)
        {
            Player = player;
            Rx = rx;
            Round = round;
        }

        [JsonProperty] public IPlayer Player { get; }
        
        public IGameFacadeRx Rx { get; }
        
        [JsonProperty] public bool AdRemoved { get; set; }

        public int State
        {
            get => Rx.State.Value;
            set => Rx.State.Value = value;
        }

        public IRound Round { get; set; }
    }
}