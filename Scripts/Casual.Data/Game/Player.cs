using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace CasualLib.Scripts.Casual.Data.Game
{
    public class Player : IPlayer
    {
        public Player([NotNull] IPlayerRx rx)
        {
            Rx = rx;
        }

        public int Score
        {
            get => Rx.Score.Value;
            set => Rx.Score.Value = value;
        }

        public int BestScore
        {
            get => Rx.BestScore.Value;
            set => Rx.BestScore.Value = value;
        }

        public int Money
        {
            get => Rx.Money.Value;
            set => Rx.Money.Value = value;
        }
        [JsonIgnore] public IPlayerRx Rx { get; }
    }
}