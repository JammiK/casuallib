using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.Interfaces.View
{
    public interface IFillRoundTimeImage
    {
        Image Image { get; }
    }
}