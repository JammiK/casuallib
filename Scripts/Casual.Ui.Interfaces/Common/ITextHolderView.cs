using TMPro;

namespace CasualLib.Scripts.Casual.Ui.Interfaces.Common
{
    public interface ITextHolderView
    {
        TextMeshProUGUI Text { get; }
    }
}