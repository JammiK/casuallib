using System;

namespace CasualLib.Scripts.Casual.Ui.Interfaces.Common
{
    public interface ITextRxView
    {
        void SetScoreStream(IObservable<int> score);
    }
}