using UnityEngine;

namespace CasualLib.Scripts.Casual.Ui.Interfaces.Common
{
    public interface IGameObjectHolder
    {
        GameObject GameObject { get; }
    }
}