using UnityEngine.UI;

namespace CasualLib.Scripts.Casual.Ui.Interfaces.Buttons
{
    public interface IButtonHolder
    {
        Button Button { get; }
    }
}