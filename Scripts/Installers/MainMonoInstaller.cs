using Installers.SubInstallers;
using Zenject;

namespace Installers
{
    public class MainMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            MainInstaller.Install(Container);
        }
    }
}