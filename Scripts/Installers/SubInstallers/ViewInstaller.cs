using CasualLib.Scripts.Casual.Ui.Interfaces.Buttons;
using CasualLib.Scripts.Casual.Ui.Interfaces.Popups;
using CasualLib.Scripts.Casual.Ui.Interfaces.Screens;
using CasualLib.Scripts.Casual.Ui.Interfaces.View;
using Zenject;

namespace Installers.SubInstallers
{
    public class ViewInstaller : Installer<ViewInstaller>
    {
        public override void InstallBindings()
        {
            Bind<IScoreView>();
            Bind<IBestScoreView>();
            Bind<IStartGameButton>();
            Bind<IPlayingMenuView>();
            Bind<IMainMenuView>();
            Bind<IFillRoundTimeImage>();
            Bind<IHardMoneyView>();
            Bind<IPausedView>();
            Bind<IResurrectPopup>();
            Bind<IResurrectButton>();
            Bind<IResurrectPriceView>();
        }

        void Bind<T>()
        {
            Container.Bind<T>().FromComponentsInHierarchy().AsSingle();
        }
    }
}