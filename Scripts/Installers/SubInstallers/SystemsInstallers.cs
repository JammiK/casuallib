using System;
using CasualLib.Scripts.Casual.Logic.Systems.GameSystems;
using CasualLib.Scripts.Casual.Logic.Systems.PlayerSystems;
using CasualLib.Scripts.Casual.Logic.Systems.Purchase;
using CasualLib.Scripts.Casual.Logic.Systems.Saves;
using CasualLib.Scripts.Casual.Logic.Systems.Socials;
using UnityEngine;
using Zenject;

namespace Installers.SubInstallers
{
    public class SystemsInstallers : Installer<SystemsInstallers>
    {
        public override void InstallBindings()
        {            
            SelfBind<ScoreSystem>().NonLazy();
            SelfBind<GameRoundSystem>().NonLazy();
            SelfBind<GameEventsSystem>().NonLazy();
            SelfBind<RoundTimeLimitSystem>().NonLazy();
            SelfBind<SocialSystem>().NonLazy();
            SelfBind<LeaderBoardSystem>().NonLazy();
            SelfBind<HardMoneySystem>().NonLazy();
            SelfBind<SaveGameSystem>().NonLazy();
            SelfBind<PurchaseSystem>().NonLazy();
            SelfBind<RemoveAdPurchaseCompleteListener>();
            SelfBind<ResurrectSystem>().NonLazy();
            SelfBind<PauseSystem>().NonLazy();
        }

        ConcreteIdArgConditionCopyNonLazyBinder SelfBind<T>()
        {
            return Container.BindInterfacesTo<T>().AsSingle();
        }
    }
}