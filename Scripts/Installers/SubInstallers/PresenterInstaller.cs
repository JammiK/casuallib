using CasualLib.Scripts.Casual.Presenters.Buttons;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Buttons;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Popups;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Screens;
using CasualLib.Scripts.Casual.Presenters.Interfaces.Views;
using CasualLib.Scripts.Casual.Presenters.Popups;
using CasualLib.Scripts.Casual.Presenters.Screens;
using CasualLib.Scripts.Casual.Presenters.Views;
using Zenject;

namespace Installers.SubInstallers
{
    public class PresenterInstaller : Installer<PresenterInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IScorePresenter>().To<ScorePresenter>().AsSingle().NonLazy();
            Container.Bind<IStartGameButtonPresenter>().To<StartGameButtonPresenter>().AsSingle().NonLazy();
            Container.Bind<IScreenPresenter>().To<MainMenuScreenPresenter>().AsSingle().NonLazy();
            Container.Bind<IScreenPresenter>().To<PlayingMenuScreenPresenter>().AsSingle().NonLazy();
            Container.Bind<IScreenPresenter>().To<LoseMenuScreenPresenter>().AsSingle().NonLazy();
            Container.Bind<IFillRoundTimeImagePresenter>().To<FillRoundTimeImagePresenter>().AsSingle().NonLazy();
            Container.Bind<IShowLeaderBoardButtonPresenter>().To<ShowLeaderBoardButtonPresenter>().AsSingle().NonLazy();
            Container.Bind<IHardMoneyPresenter>().To<HardMoneyPresenter>().AsSingle().NonLazy();
            Container.Bind<IResurrectPopupPresenter>().To<ResurrectPopupPresenter>().AsSingle().NonLazy();
            Container.Bind<IResurrectButtonPresenter>().To<ResurrectButtonPresenter>().AsSingle().NonLazy();
            Container.Bind<IPausedViewPresenter>().To<PausedViewPresenter>().AsSingle().NonLazy();
            Container.Bind<IResurrectPricePresenter>().To<ResurrectPricePresenter>().AsSingle().NonLazy();
        }
    }
}