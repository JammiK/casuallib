using CasualLib.Scripts.Casual.Logic.Interfaces.Repositories;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.AdServices;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.InputServices;
using CasualLib.Scripts.Casual.Logic.Interfaces.Services.Saves;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.AdSystems;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.Debug;
using CasualLib.Scripts.Casual.Logic.Interfaces.Systems.GameSystems;
using CasualLib.Scripts.Casual.Logic.Repositories;
using CasualLib.Scripts.Casual.Logic.Services.AdServices;
using CasualLib.Scripts.Casual.Logic.Services.InputServices;
using CasualLib.Scripts.Casual.Logic.Services.Saves;
using CasualLib.Scripts.Casual.Logic.Systems.AdSystems;
using CasualLib.Scripts.Casual.Logic.Systems.Debug;
using CasualLib.Scripts.Casual.Logic.Systems.GameSystems;
using Zenject;

namespace CasualLib.Scripts.Installers.SubInstallers
{
    public class LogicInstaller : Installer<LogicInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<ITapService>().To<TapService>().AsSingle().Lazy();
            Container.Bind<IDoubleTapService>().To<DoubleTapService>().AsSingle().Lazy();
            Container.Bind<IGameStateSystem>().To<GameStateSystem>().AsSingle();
            Container.Bind<IGameEventsLogger>().To<GameEventsLogger>().AsSingle().NonLazy();
            Container.Bind<ILoseSystem>().To<LoseSystem>().AsSingle().NonLazy();
            Container.Bind<IAdService>().To<UnityAdService>().AsSingle();
            Container.Bind<IAdSystem>().To<AdSystem>().AsSingle().NonLazy();
            Container.Bind<ISaveGameService>().To<SaveGameService>().AsSingle().Lazy();
            Container.Bind<IPurchaseRepository>().To<PurchaseRepository>().AsSingle().Lazy();
        }
    }
}