using CasualLib.Scripts.Casual.Data.Game;
using CasualLib.Scripts.Casual.Data.Game.Rx;
using CasualLib.Scripts.Casual.Data.Interfaces.Game;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Constants;
using CasualLib.Scripts.Casual.Data.Interfaces.Game.Rx;
using CasualLib.Scripts.Casual.Data.Interfaces.Purchase;
using CasualLib.Scripts.Casual.Data.Purchase;
using Zenject;

namespace Installers.SubInstallers
{
    public class DataInstallers : Installer<DataInstallers>
    {
        const int DefaultPlayerScore = 0;
        const int DefaultPlayerBestScore = 0;
        const int DefaultPlayerMoney = 0;
        const int DefaultRound = 0;
        
        public override void InstallBindings()
        {
            Container.Bind<IGameFacade>().To<GameFacade>().AsSingle();
            Container.Bind<IPlayer>().To<Player>().AsSingle();
            Container.Bind<IRound>().To<Round>().AsSingle();
            Container.Bind<IGameSettings>().To<GameSettings>().AsSingle();
            Container.Bind<IPurchaseContainer>().To<PurchaseContainer>().AsSingle();
            
            InstallNewGame();
        }

        void InstallNewGame()
        {
            Container.Bind<IPlayerRx>().FromInstance(
                new PlayerRx(DefaultPlayerScore, DefaultPlayerBestScore, DefaultPlayerMoney))
                .AsSingle();
            
            Container.Bind<IGameFacadeRx>()
                .FromInstance(new GameFacadeRx(GameStateConstants.MainMenu))
                .AsSingle();
            
            Container.Bind<IRoundRx>()
                .FromInstance(new RoundRx())
                .AsSingle();
        }
    }
}