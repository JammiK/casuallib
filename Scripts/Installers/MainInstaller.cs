using System;
using CasualLib.Scripts.Installers.SubInstallers;
using Installers.SubInstallers;
using Zenject;

namespace Installers
{
    public class MainInstaller : Installer<MainInstaller>
    {
        public override void InstallBindings()
        {
            DataInstallers.Install(Container);
            LogicInstaller.Install(Container);
            SystemsInstallers.Install(Container);
            PresenterInstaller.Install(Container);
            ViewInstaller.Install(Container);
        }
    }
}